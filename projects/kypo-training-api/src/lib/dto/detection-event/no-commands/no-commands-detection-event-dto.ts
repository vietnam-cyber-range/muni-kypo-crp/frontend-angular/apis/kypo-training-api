import { DetectionEventDTO } from '../detection-event-dto';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface NoCommandsDetectionEventDTO extends DetectionEventDTO {}
