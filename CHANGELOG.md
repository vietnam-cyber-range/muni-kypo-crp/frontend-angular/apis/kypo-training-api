### 16.0.0 Update to Angular 16.
* 2f3ea3c -- [CI/CD] Update packages.json version based on GitLab tag.
* 571d5a6 -- Merge branch '73-update-to-angular-16' into 'master'
* a8d12fb -- Update to Angular 16
### 15.1.6 Add answerRequired field to adaptive question dto.
* 6d1cf19 -- [CI/CD] Update packages.json version based on GitLab tag.
* 079c66c -- Merge branch 'develop' into 'master'
* f00b517 -- Merge branch '72-add-required-field-to-adaptive-question' into 'develop'
* 3ffbc5b -- Update packages
* f4ab1d0 -- Update version
* 3e5ad8f -- Merge branch 'develop' into 72-add-required-field-to-adaptive-question
* fe34a2d -- Fix naming
* 15b7773 -- Merge branch 'master' into 72-add-required-field-to-adaptive-question
* 2294513 -- Add required field to adaptive question
### 15.1.5 Add filter logic to detection events.
* 8ec9217 -- [CI/CD] Update packages.json version based on GitLab tag.
* 5c94d18 -- Merge branch 'develop' into 'master'
* 2185b93 -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* cc96742 -- Forbidden commands detection method integration
### 15.1.4 Add run id to detection event dto.
* 5616269 -- [CI/CD] Update packages.json version based on GitLab tag.
* faafd5d -- Merge branch 'develop' into 'master'
* 92f292f -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* 3152980 -- Forbidden commands detection method integration
### 15.1.3 Add level order to detection event.
* 4c3d4b6 -- [CI/CD] Update packages.json version based on GitLab tag.
* 7611476 -- Merge branch 'develop' into 'master'
* e5aa2ab -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* a87b562 -- Forbidden commands detection method integration
### 15.1.2 Fix detected forbidden command dto type.
* 3b033c3 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4b37cd3 -- Merge branch 'develop' into 'master'
* 30a04db -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* abd9ee6 -- Forbidden commands detection method integration
### 15.1.1 Add hostname and occurredAt to detected forbidden command.
* 6686f39 -- [CI/CD] Update packages.json version based on GitLab tag.
* 6e6e5a2 -- Merge branch 'develop' into 'master'
* dab057c -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* 586ea85 -- Forbidden commands detection method integration
### 15.1.0 Update cheating detection api with updated models and requests.
* 08c6afb -- [CI/CD] Update packages.json version based on GitLab tag.
* bcdc9dd -- Merge branch 'develop' into 'master'
* 99279f6 -- Merge branch 'master' into 'develop'
* 548675e -- Merge branch 'forbidden-commands-detection-method-integration' into 'develop'
* b272087 -- Update version
* 7f3a7d7 -- revert package-lock changes
* 8ef1ec7 -- Update package
* 1209da1 -- Update version
* 12d33c8 -- Update rest endpoint map
* 247584c -- Add training run id to forbidden detection event
* 97df70e -- Update condition
* 08057bf -- Merge branch 'develop' into forbidden-commands-detection-method-integration
* 3edfbc0 -- Update api paths
* 9e55a86 -- Update api paths
* ab82e60 -- Update detection event api and add forbidden commands support
* ca928ad -- Implement missing models and api functions for forbidden commands
* b388362 -- Unfinished changes to cheating detection api
* cbe1aac -- Update detection event model
* b448336 -- Updated api service for forbidden commands.
* a5390ac -- Updated cheating detection and forbidden commands mappers
### 15.0.3 Fix nullability of training run allocation id.
* dda4b4b -- [CI/CD] Update packages.json version based on GitLab tag.
* eab2a07 -- Merge branch 'fix-nullability-of-allocation-id' into 'master'
* e155bbc -- Fix nullability
### 15.0.2 Enhance accessed training run api call.
* 264fd5d -- [CI/CD] Update packages.json version based on GitLab tag.
* 066a9cb -- Merge branch 'develop' into 'master'
*   52324a5 -- Merge branch '68-add-support-for-training-run-table-sorting' into 'develop'
|\  
| * f64ad0a -- Update version
| *   8ec83b5 -- Merge develop
| |\  
| |/  
|/|   
* | 9577a41 -- Merge branch '69-add-allocation-id-to-training-run' into 'develop'
* | b5552b5 -- Update package lock
* | 2572f22 -- Fix package lock
* | 53506a1 -- Update model package
* | 847af97 -- Update version
* | 70058ae -- Merge branch 'develop' into 69-add-allocation-id-to-training-run
* | 76fe666 -- Add sandbox instance allocation id to training run
 /  
* 3ec1efe -- Fix package-lock
* 562e90b -- Add support for accessed runs table sorting.
### 15.0.0 Update to Angular 15.
* d243071 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6718c8d -- Merge new package.json and Angular 15
|\  
| * 8659e95 -- Merge branch '71-update-to-angular-15' into 'master'
| *   12d5088 -- Merge branch '71-update-to-angular-15' of gitlab.ics.muni.cz:muni-kypo-crp/frontend-angular/apis/kypo-training-api into 71-update-to-angular-15
| |\  
| | * 67ca857 -- Update to Angular 15
| * 4139bbd -- Update to Angular 15
* ae12bff -- Update optional dependencies
### 14.2.4 Update api with new training definition model.
* 649c124 -- [CI/CD] Update packages.json version based on GitLab tag.
* ce2d390 -- Merge branch 'develop' into 'master'
* ed12c1f -- Merge branch '70-adjust-api-with-changes-from-new-training-definition-model' into 'develop'
* fd8f550 -- Bump version of training model
* 6ac703d -- Update api with new training definition model.
### 14.2.3 Bump model version.
* 892b569 -- [CI/CD] Update packages.json version based on GitLab tag.
* 20b2df6 -- Merge branch 'develop' into 'master'
* dcd40b5 -- Bump version of model.
### 14.2.2 Added cheating detection archive api method.
* e741359 -- [CI/CD] Update packages.json version based on GitLab tag.
* 601a9ea -- Merge branch 'develop' into 'master'
* a99ef8c -- Added cheating detection api archive method.
### 14.2.1 Updated and enhanced apis, DTOs and mappers for experimental version of cheating detection.
* 395dc7e -- [CI/CD] Update packages.json version based on GitLab tag.
* f41680b -- Merge branch 'cheating-detection' into 'master'
* 47748a5 -- Removed unused attribute. Disabled linter for empty interface.
* f8ea295 -- Removed console.log().
* 416af80 -- Updated version and packages.
* 6005b13 -- Merge changes from cheating detection fork.
* 7ab983f -- Merge branch 'master' into cheating-detection
* 682995c -- Fixed cheating detection create routing. Added a type to paginated resource of abstract event.
### 14.2.0 Replaced sandbox id with sandbox uuid.
* 7dbd228 -- [CI/CD] Update packages.json version based on GitLab tag.
* d84f682 -- Merge branch 'fix-sandbox-uuid' into 'master'
* e883296 -- Fix sandbox uuid
### 14.1.0 Add apis, DTOs and mappers for experimental version of cheating detection.
* bd371d2 -- [CI/CD] Update packages.json version based on GitLab tag.
* dfa0db8 -- Merge branch 'cheating-detection' into 'master'
* 61d99cf -- Cheating detection
### 14.0.3 Added attributes to TrainingRun stating whether logging of events and commands works.
* 34cb6f0 -- [CI/CD] Update packages.json version based on GitLab tag.
* 82083c0 -- Merge branch '67-add-attribute-stating-whether-event-and-command-logging-works' into 'master'
* f045067 -- Resolve "Add attribute stating whether event and command logging works"
### 14.0.2 Exported score from training instance is now uncompressed
* 9866f6d -- [CI/CD] Update packages.json version based on GitLab tag.
* 0578927 -- Merge branch '66-update-exportscore-to-receive-uncompressed-data' into 'master'
* 744a003 -- Updated headers
### 14.0.1 Added functionality to export scores from training instance.
* badeae9 -- [CI/CD] Update packages.json version based on GitLab tag.
* f9eb8f2 -- Merge branch '65-export-score-of-all-trainees-from-training-instance' into 'master'
* 277c93a -- Added functionality to export scores from training instance
### 14.0.0 Update to Angular 14.
* 7c43bd5 -- [CI/CD] Update packages.json version based on GitLab tag.
* 323e8bb -- Merge branch '64-update-to-angular-14' into 'master'
* 9ef18bc -- Resolve "Update to Angular 14"
### 13.0.8 Added field for required commands in training level.
* 6b3feeb -- [CI/CD] Update packages.json version based on GitLab tag.
* 7bd8fe1 -- Merge branch '63-add-attribute-commands_required-to-training-level-dto-classes' into 'master'
* e542292 -- Added attribute commands_required to training level dto classes and updated mapping
### 13.0.7 Export phase mapper and abstract phase dto for from public api.
* 25aa298 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4037002 -- Merge branch 'adaptive-model-simulator-adjustments' into 'master'
* 2a98eef -- Adaptive model simulator adjustments
### 13.0.6 Added field that marks if current level of training run has been answered.
* 4a2a78c -- [CI/CD] Update packages.json version based on GitLab tag.
* 23c1a82 -- Merge branch '62-add-fields-phase_answered-and-level_answered-to-accesstrainingrundto' into 'master'
* 67468f4 -- Resolve "Add fields phase_answered and level_answered to AccessTrainingRunDTO"
### 13.0.5 Fix MITRE techniques mapping.
* 4c21a0c -- [CI/CD] Update packages.json version based on GitLab tag.
* e0df389 -- Merge branch '61-fix-mitre-techniques-mapping' into 'master'
* 881dfb5 -- Resolve "Fix mitre techniques mapping"
### 13.0.4 Added support for movement between already finished levels.
* 9c6977e -- [CI/CD] Update packages.json version based on GitLab tag.
* d76bd76 -- Merge branch '60-add-necessary-fields-and-update-mappers-to-support-movement-between-accessed-levels-phases' into 'master'
* e940be7 -- Resolve "Add necessary fields and update mappers to support movement between accessed levels/phases during training run"
### 13.0.3 Added mitre techniques field.
* f4360b9 -- [CI/CD] Update packages.json version based on GitLab tag.
* 59f6709 -- Merge branch '58-add-support-for-map-of-games' into 'master'
* 64d2f60 -- Resolve "Add support for map of games"
### 13.0.2 Added new field minimal possible solve time added to the assessment and training level.
* 47f328d -- [CI/CD] Update packages.json version based on GitLab tag.
* e4ef90b -- Merge branch '13.x.x-pre-tag-changes' into 'master'
* 9fb3211 -- 13.x.x pre tag changes
### 13.0.1 Add has reference solution field.
* ea49761 -- [CI/CD] Update packages.json version based on GitLab tag.
* 62c5bda -- Merge branch '56-add-has-reference-solution-field' into 'master'
* 30e4c03 -- Resolve "Add has reference solution field"
### 13.0.0 Update to Angular 13, CI/CD optimisation, local env. and sandbox def. id attribute added to TI and TR DTOs, access level/phase integrated.
* 548913a -- [CI/CD] Update packages.json version based on GitLab tag.
*   d64977d -- Merge branch '12.1.1-pre-tag-changes' into 'master'
|\  
| * c55a5ba -- 12.1.1 pre tag changes
|/  
* 9a7c808 -- Merge branch '53-update-to-angular-13' into 'master'
* 313f5af -- Resolve "Update to Angular 13"
### 12.1.0 Add tests for particular api services. Add DTO, mappers and api methods for command analysis visualization.
* 842c40c -- [CI/CD] Update packages.json version based on GitLab tag.
*   14317ec -- Merge branch '47-integrate-command-visualization' into 'master'
|\  
| * 8962094 -- Resolve "Integrate command visualization"
|/  
*   0a87b7c -- Merge branch '46-create-tests-for-api-services-and-mappers' into 'master'
|\  
| * 3ce3c9b -- Resolve "Create tests for api services and mappers"
|/  
* 5f94352 -- Merge branch '50-add-license' into 'master'
* 729a37b -- Add license file
### 12.0.7 Add variant_answers into training level dto. Add training run info dto, mappers and api methods.
* bc85d33 -- [CI/CD] Update packages.json version based on GitLab tag.
*   91e4b37 -- Merge branch '49-add-training-run-info-dto-and-api-call' into 'master'
|\  
| * cff6afc -- Resolve "Add training run info dto and api call"
|/  
* 6f2ab06 -- Merge branch '48-move-attribute-variant_sandboxes-from-definition-to-training-level-as-variant_answers' into 'master'
* b88287f -- Removed variant_sandboxes from definition dto. Added variant_answers into...
### 12.0.6 Add attribute default_content to Training Definition Create DTO.
* decbbcd -- [CI/CD] Update packages.json version based on GitLab tag.
* 351d267 -- Merge branch '44-add-attribute-default_content-to-trainingdefinitioncreatedto' into 'master'
* 70ded82 -- Update .gitlab-ci.yml file
* 8303b40 -- Bump version file
* f971dcf -- Merge remote-tracking branch 'origin/master' into 44-add-attribute-default_content-to-trainingdefinitioncreatedto
* 89e4e5a -- Bump version of model
* 2063e9d -- Added attribute default_content to training definition create DTO. Modification of the mapper accordingly.
### 12.0.5 Add last edit by field.
* 3be52bc -- [CI/CD] Update packages.json version based on GitLab tag.
*   450a99b -- Merge branch '45-add-last-edit-by-field' into 'master'
|\  
| * b2ee829 -- Resolve "Add last edit by field"
|/  
* 31a83c2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9b81e0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   25f22b8 -- Merge branch '43-create-tag-with-latest-changes' into 'master'
|\  
| * 45622c2 -- Tag notes
|/  
*   1bbc847 -- Merge branch '42-allow-save-of-multiple-levels-at-once' into 'master'
|\  
| * 0b8b9c1 -- Resolve "Allow save of multiple levels at once"
|/  
*   08401eb -- Merge branch '41-add-node-modules-to-gitignore' into 'master'
|\  
| * d9a35e1 -- Add node_modules to gitignore
* |   fc3da2b -- Merge branch '40-add-id-to-training-definition-title-in-trainingdefinitioninfo' into 'master'
|\ \  
| |/  
|/|   
| * cf5fbc3 -- Add id
|/  
* cc35d85 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c56be4d -- [CI/CD] Update packages.json version based on GitLab tag.
*   3bbef5a -- Merge branch '35-modify-dtos-and-mappers-needed-for-apg-integration' into 'master'
|\  
| * 569adca -- Changed version of the project to 12.0.3
| * faac130 -- Changed version of the training model to 12.0.5.
| *   3c12a78 -- Merged with master
| |\  
| |/  
|/|   
* | fa44a6b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 5c35b37 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5e2e01f -- Merge branch '39-rename-game-level-to-training-level-and-bump-version-of-kypo-training-model' into 'master'
|\ \  
| * | d90c452 -- Resolve "Rename game level to training level and bump version of kypo-training-model"
|/ /  
* |   55b377b -- Merge branch '38-rename-the-attribute-flag-to-the-answer' into 'master'
|\ \  
| * | 614cd21 -- Resolve "Rename the attribute flag to the answer"
|/ /  
* |   4777782 -- Merge branch '36-bump-version-of-sentinel' into 'master'
|\ \  
| * | 7237bb1 -- Resolve "Bump version of Sentinel"
|/ /  
| * b9a51ff -- Renaming flagIdentifier to flagVariableName and variantAnswers to variantSandboxes.
| * da8067e -- Added attribute variantAnswers and modify mappers accordingly.
| * 3d6030f -- Added attribute variantAnswers and modify mappers accordingly.
|/  
* 48a8506 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* eed7790 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9d26cd3 -- Merge branch '34-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c5fdbfc -- Update gitlab CI
|/  
* 7153b7d -- Update project package.json version based on GitLab tag. Done by CI
*   6fdbe4f -- Merge branch '33-update-to-angular-12' into 'master'
|\  
| * 73201e0 -- Update to Angular 12
|/  
* a94cc6a -- Update project package.json version based on GitLab tag. Done by CI
*   c53ba3d -- Merge branch '31-adjust-dtos-and-mappers-according-to-the-new-design-of-questions-in-the-assessment-levels' into 'master'
|\  
| * b7991dc -- Resolve "Adjust DTOs and mappers according to the new design of questions in the assessment levels"
|/  
* 25e1cc9 -- Update project package.json version based on GitLab tag. Done by CI
*   2af45f3 -- Merge branch '32-fix-task-copy-url' into 'master'
|\  
| * 05d40d5 -- Copy task url fixed
|/  
* 16ff17f -- Update project package.json version based on GitLab tag. Done by CI
*   85daa0d -- Merge branch '30-fix-new-line-template-string-blank-spaces' into 'master'
|\  
| * 7b7ff92 -- Fix new line problem
|/  
*   b8ba84b -- Merge branch '29-integrate-adaptive-learning-techniques' into 'master'
|\  
| * e490310 -- Resolve "Integrate Adaptive learning techniques"
|/  
* 63442f9 -- Update project package.json version based on GitLab tag. Done by CI
*   cd242e5 -- Merge branch '27-update-to-angular-11' into 'master'
|\  
| * f79e59b -- Resolve "Update to Angular 11"
|/  
*   04e38d8 -- Merge branch '26-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * e96d821 -- recreate package lock
|/  
* 659a048 -- Update project package.json version based on GitLab tag. Done by CI
*   70b4731 -- Merge branch '25-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 88b2103 -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   2170243 -- Merge branch '24-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 395b55e -- Migrate to eslint, fix lint warnings/errors
|/  
* 0001a11 -- Update project package.json version based on GitLab tag. Done by CI
*   864ec01 -- Merge branch '23-update-dependencies-to-new-format' into 'master'
|\  
| * af218bf -- Update dependencies
|/  
* c27550e -- Update project package.json version based on GitLab tag. Done by CI
*   59633bf -- Merge branch '22-rename-to-kypo-training-api' into 'master'
|\  
| * 9dfc3de -- Rename package
|/  
* f74c4d3 -- Update project package.json version based on GitLab tag. Done by CI
*   699daab -- Merge branch '20-update-endpoint-for-training-events' into 'master'
|\  
| * 394bae6 -- Changed training event uri to match new backend endpoint
|/  
*   0de3e14 -- Merge branch '19-use-cypress-image-in-ci' into 'master'
|\  
| * 4c7c5e9 -- Resolve "Use cypress image in CI"
|/  
* a0f5c7d -- Update project package.json version based on GitLab tag. Done by CI
* 7676143 -- Update project package.json version based on GitLab tag. Done by CI
*   fcacdff -- Merge branch '18-remove-deep-import-from-rxjs' into 'master'
|\  
| * f22ac6c -- Remove deep imports from rxjs
|/  
* d5310e4 -- Update project package.json version based on GitLab tag. Done by CI
*   eff306f -- Merge branch '17-replace-dependency-on-kypo-common-with-sentinel-common' into 'master'
|\  
| * 2c0061a -- Replace kypo-common with @sentinel/common
|/  
* 2416f07 -- Update project package.json version based on GitLab tag. Done by CI
*   aca94b8 -- Merge branch '16-update-to-angular-10' into 'master'
|\  
| * ba42b44 -- Resolve "Update to Angular 10"
|/  
* 288c5da -- Update project package.json version based on GitLab tag. Done by CI
### 12.0.4 Include id to training definition info title. Allow save of multiple levels/phases at once.
* 9b81e0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   25f22b8 -- Merge branch '43-create-tag-with-latest-changes' into 'master'
|\  
| * 45622c2 -- Tag notes
|/  
*   1bbc847 -- Merge branch '42-allow-save-of-multiple-levels-at-once' into 'master'
|\  
| * 0b8b9c1 -- Resolve "Allow save of multiple levels at once"
|/  
*   08401eb -- Merge branch '41-add-node-modules-to-gitignore' into 'master'
|\  
| * d9a35e1 -- Add node_modules to gitignore
* |   fc3da2b -- Merge branch '40-add-id-to-training-definition-title-in-trainingdefinitioninfo' into 'master'
|\ \  
| |/  
|/|   
| * cf5fbc3 -- Add id
|/  
* cc35d85 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c56be4d -- [CI/CD] Update packages.json version based on GitLab tag.
*   3bbef5a -- Merge branch '35-modify-dtos-and-mappers-needed-for-apg-integration' into 'master'
|\  
| * 569adca -- Changed version of the project to 12.0.3
| * faac130 -- Changed version of the training model to 12.0.5.
| *   3c12a78 -- Merged with master
| |\  
| |/  
|/|   
* | fa44a6b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 5c35b37 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5e2e01f -- Merge branch '39-rename-game-level-to-training-level-and-bump-version-of-kypo-training-model' into 'master'
|\ \  
| * | d90c452 -- Resolve "Rename game level to training level and bump version of kypo-training-model"
|/ /  
* |   55b377b -- Merge branch '38-rename-the-attribute-flag-to-the-answer' into 'master'
|\ \  
| * | 614cd21 -- Resolve "Rename the attribute flag to the answer"
|/ /  
* |   4777782 -- Merge branch '36-bump-version-of-sentinel' into 'master'
|\ \  
| * | 7237bb1 -- Resolve "Bump version of Sentinel"
|/ /  
| * b9a51ff -- Renaming flagIdentifier to flagVariableName and variantAnswers to variantSandboxes.
| * da8067e -- Added attribute variantAnswers and modify mappers accordingly.
| * 3d6030f -- Added attribute variantAnswers and modify mappers accordingly.
|/  
* 48a8506 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* eed7790 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9d26cd3 -- Merge branch '34-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c5fdbfc -- Update gitlab CI
|/  
* 7153b7d -- Update project package.json version based on GitLab tag. Done by CI
*   6fdbe4f -- Merge branch '33-update-to-angular-12' into 'master'
|\  
| * 73201e0 -- Update to Angular 12
|/  
* a94cc6a -- Update project package.json version based on GitLab tag. Done by CI
*   c53ba3d -- Merge branch '31-adjust-dtos-and-mappers-according-to-the-new-design-of-questions-in-the-assessment-levels' into 'master'
|\  
| * b7991dc -- Resolve "Adjust DTOs and mappers according to the new design of questions in the assessment levels"
|/  
* 25e1cc9 -- Update project package.json version based on GitLab tag. Done by CI
*   2af45f3 -- Merge branch '32-fix-task-copy-url' into 'master'
|\  
| * 05d40d5 -- Copy task url fixed
|/  
* 16ff17f -- Update project package.json version based on GitLab tag. Done by CI
*   85daa0d -- Merge branch '30-fix-new-line-template-string-blank-spaces' into 'master'
|\  
| * 7b7ff92 -- Fix new line problem
|/  
*   b8ba84b -- Merge branch '29-integrate-adaptive-learning-techniques' into 'master'
|\  
| * e490310 -- Resolve "Integrate Adaptive learning techniques"
|/  
* 63442f9 -- Update project package.json version based on GitLab tag. Done by CI
*   cd242e5 -- Merge branch '27-update-to-angular-11' into 'master'
|\  
| * f79e59b -- Resolve "Update to Angular 11"
|/  
*   04e38d8 -- Merge branch '26-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * e96d821 -- recreate package lock
|/  
* 659a048 -- Update project package.json version based on GitLab tag. Done by CI
*   70b4731 -- Merge branch '25-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 88b2103 -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   2170243 -- Merge branch '24-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 395b55e -- Migrate to eslint, fix lint warnings/errors
|/  
* 0001a11 -- Update project package.json version based on GitLab tag. Done by CI
*   864ec01 -- Merge branch '23-update-dependencies-to-new-format' into 'master'
|\  
| * af218bf -- Update dependencies
|/  
* c27550e -- Update project package.json version based on GitLab tag. Done by CI
*   59633bf -- Merge branch '22-rename-to-kypo-training-api' into 'master'
|\  
| * 9dfc3de -- Rename package
|/  
* f74c4d3 -- Update project package.json version based on GitLab tag. Done by CI
*   699daab -- Merge branch '20-update-endpoint-for-training-events' into 'master'
|\  
| * 394bae6 -- Changed training event uri to match new backend endpoint
|/  
*   0de3e14 -- Merge branch '19-use-cypress-image-in-ci' into 'master'
|\  
| * 4c7c5e9 -- Resolve "Use cypress image in CI"
|/  
* a0f5c7d -- Update project package.json version based on GitLab tag. Done by CI
* 7676143 -- Update project package.json version based on GitLab tag. Done by CI
*   fcacdff -- Merge branch '18-remove-deep-import-from-rxjs' into 'master'
|\  
| * f22ac6c -- Remove deep imports from rxjs
|/  
* d5310e4 -- Update project package.json version based on GitLab tag. Done by CI
*   eff306f -- Merge branch '17-replace-dependency-on-kypo-common-with-sentinel-common' into 'master'
|\  
| * 2c0061a -- Replace kypo-common with @sentinel/common
|/  
* 2416f07 -- Update project package.json version based on GitLab tag. Done by CI
*   aca94b8 -- Merge branch '16-update-to-angular-10' into 'master'
|\  
| * ba42b44 -- Resolve "Update to Angular 10"
|/  
* 288c5da -- Update project package.json version based on GitLab tag. Done by CI
### 12.0.3 Addition of the attributes for APG.
* c56be4d -- [CI/CD] Update packages.json version based on GitLab tag.
*   3bbef5a -- Merge branch '35-modify-dtos-and-mappers-needed-for-apg-integration' into 'master'
|\  
| * 569adca -- Changed version of the project to 12.0.3
| * faac130 -- Changed version of the training model to 12.0.5.
| *   3c12a78 -- Merged with master
| |\  
| |/  
|/|   
* | fa44a6b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 5c35b37 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   5e2e01f -- Merge branch '39-rename-game-level-to-training-level-and-bump-version-of-kypo-training-model' into 'master'
|\ \  
| * | d90c452 -- Resolve "Rename game level to training level and bump version of kypo-training-model"
|/ /  
* |   55b377b -- Merge branch '38-rename-the-attribute-flag-to-the-answer' into 'master'
|\ \  
| * | 614cd21 -- Resolve "Rename the attribute flag to the answer"
|/ /  
* |   4777782 -- Merge branch '36-bump-version-of-sentinel' into 'master'
|\ \  
| * | 7237bb1 -- Resolve "Bump version of Sentinel"
|/ /  
| * b9a51ff -- Renaming flagIdentifier to flagVariableName and variantAnswers to variantSandboxes.
| * da8067e -- Added attribute variantAnswers and modify mappers accordingly.
| * 3d6030f -- Added attribute variantAnswers and modify mappers accordingly.
|/  
* 48a8506 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* eed7790 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9d26cd3 -- Merge branch '34-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c5fdbfc -- Update gitlab CI
|/  
* 7153b7d -- Update project package.json version based on GitLab tag. Done by CI
*   6fdbe4f -- Merge branch '33-update-to-angular-12' into 'master'
|\  
| * 73201e0 -- Update to Angular 12
|/  
* a94cc6a -- Update project package.json version based on GitLab tag. Done by CI
*   c53ba3d -- Merge branch '31-adjust-dtos-and-mappers-according-to-the-new-design-of-questions-in-the-assessment-levels' into 'master'
|\  
| * b7991dc -- Resolve "Adjust DTOs and mappers according to the new design of questions in the assessment levels"
|/  
* 25e1cc9 -- Update project package.json version based on GitLab tag. Done by CI
*   2af45f3 -- Merge branch '32-fix-task-copy-url' into 'master'
|\  
| * 05d40d5 -- Copy task url fixed
|/  
* 16ff17f -- Update project package.json version based on GitLab tag. Done by CI
*   85daa0d -- Merge branch '30-fix-new-line-template-string-blank-spaces' into 'master'
|\  
| * 7b7ff92 -- Fix new line problem
|/  
*   b8ba84b -- Merge branch '29-integrate-adaptive-learning-techniques' into 'master'
|\  
| * e490310 -- Resolve "Integrate Adaptive learning techniques"
|/  
* 63442f9 -- Update project package.json version based on GitLab tag. Done by CI
*   cd242e5 -- Merge branch '27-update-to-angular-11' into 'master'
|\  
| * f79e59b -- Resolve "Update to Angular 11"
|/  
*   04e38d8 -- Merge branch '26-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * e96d821 -- recreate package lock
|/  
* 659a048 -- Update project package.json version based on GitLab tag. Done by CI
*   70b4731 -- Merge branch '25-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 88b2103 -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   2170243 -- Merge branch '24-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 395b55e -- Migrate to eslint, fix lint warnings/errors
|/  
* 0001a11 -- Update project package.json version based on GitLab tag. Done by CI
*   864ec01 -- Merge branch '23-update-dependencies-to-new-format' into 'master'
|\  
| * af218bf -- Update dependencies
|/  
* c27550e -- Update project package.json version based on GitLab tag. Done by CI
*   59633bf -- Merge branch '22-rename-to-kypo-training-api' into 'master'
|\  
| * 9dfc3de -- Rename package
|/  
* f74c4d3 -- Update project package.json version based on GitLab tag. Done by CI
*   699daab -- Merge branch '20-update-endpoint-for-training-events' into 'master'
|\  
| * 394bae6 -- Changed training event uri to match new backend endpoint
|/  
*   0de3e14 -- Merge branch '19-use-cypress-image-in-ci' into 'master'
|\  
| * 4c7c5e9 -- Resolve "Use cypress image in CI"
|/  
* a0f5c7d -- Update project package.json version based on GitLab tag. Done by CI
* 7676143 -- Update project package.json version based on GitLab tag. Done by CI
*   fcacdff -- Merge branch '18-remove-deep-import-from-rxjs' into 'master'
|\  
| * f22ac6c -- Remove deep imports from rxjs
|/  
* d5310e4 -- Update project package.json version based on GitLab tag. Done by CI
*   eff306f -- Merge branch '17-replace-dependency-on-kypo-common-with-sentinel-common' into 'master'
|\  
| * 2c0061a -- Replace kypo-common with @sentinel/common
|/  
* 2416f07 -- Update project package.json version based on GitLab tag. Done by CI
*   aca94b8 -- Merge branch '16-update-to-angular-10' into 'master'
|\  
| * ba42b44 -- Resolve "Update to Angular 10"
|/  
* 288c5da -- Update project package.json version based on GitLab tag. Done by CI
### 12.0.2 Rename flag to answer, rename game level to training level, new version of Sentinel.
* 5c35b37 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5e2e01f -- Merge branch '39-rename-game-level-to-training-level-and-bump-version-of-kypo-training-model' into 'master'
|\  
| * d90c452 -- Resolve "Rename game level to training level and bump version of kypo-training-model"
|/  
*   55b377b -- Merge branch '38-rename-the-attribute-flag-to-the-answer' into 'master'
|\  
| * 614cd21 -- Resolve "Rename the attribute flag to the answer"
|/  
*   4777782 -- Merge branch '36-bump-version-of-sentinel' into 'master'
|\  
| * 7237bb1 -- Resolve "Bump version of Sentinel"
|/  
* 48a8506 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* eed7790 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9d26cd3 -- Merge branch '34-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c5fdbfc -- Update gitlab CI
|/  
* 7153b7d -- Update project package.json version based on GitLab tag. Done by CI
*   6fdbe4f -- Merge branch '33-update-to-angular-12' into 'master'
|\  
| * 73201e0 -- Update to Angular 12
|/  
* a94cc6a -- Update project package.json version based on GitLab tag. Done by CI
*   c53ba3d -- Merge branch '31-adjust-dtos-and-mappers-according-to-the-new-design-of-questions-in-the-assessment-levels' into 'master'
|\  
| * b7991dc -- Resolve "Adjust DTOs and mappers according to the new design of questions in the assessment levels"
|/  
* 25e1cc9 -- Update project package.json version based on GitLab tag. Done by CI
*   2af45f3 -- Merge branch '32-fix-task-copy-url' into 'master'
|\  
| * 05d40d5 -- Copy task url fixed
|/  
* 16ff17f -- Update project package.json version based on GitLab tag. Done by CI
*   85daa0d -- Merge branch '30-fix-new-line-template-string-blank-spaces' into 'master'
|\  
| * 7b7ff92 -- Fix new line problem
|/  
*   b8ba84b -- Merge branch '29-integrate-adaptive-learning-techniques' into 'master'
|\  
| * e490310 -- Resolve "Integrate Adaptive learning techniques"
|/  
* 63442f9 -- Update project package.json version based on GitLab tag. Done by CI
*   cd242e5 -- Merge branch '27-update-to-angular-11' into 'master'
|\  
| * f79e59b -- Resolve "Update to Angular 11"
|/  
*   04e38d8 -- Merge branch '26-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * e96d821 -- recreate package lock
|/  
* 659a048 -- Update project package.json version based on GitLab tag. Done by CI
*   70b4731 -- Merge branch '25-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 88b2103 -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   2170243 -- Merge branch '24-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 395b55e -- Migrate to eslint, fix lint warnings/errors
|/  
* 0001a11 -- Update project package.json version based on GitLab tag. Done by CI
*   864ec01 -- Merge branch '23-update-dependencies-to-new-format' into 'master'
|\  
| * af218bf -- Update dependencies
|/  
* c27550e -- Update project package.json version based on GitLab tag. Done by CI
*   59633bf -- Merge branch '22-rename-to-kypo-training-api' into 'master'
|\  
| * 9dfc3de -- Rename package
|/  
* f74c4d3 -- Update project package.json version based on GitLab tag. Done by CI
*   699daab -- Merge branch '20-update-endpoint-for-training-events' into 'master'
|\  
| * 394bae6 -- Changed training event uri to match new backend endpoint
|/  
*   0de3e14 -- Merge branch '19-use-cypress-image-in-ci' into 'master'
|\  
| * 4c7c5e9 -- Resolve "Use cypress image in CI"
|/  
* a0f5c7d -- Update project package.json version based on GitLab tag. Done by CI
* 7676143 -- Update project package.json version based on GitLab tag. Done by CI
*   fcacdff -- Merge branch '18-remove-deep-import-from-rxjs' into 'master'
|\  
| * f22ac6c -- Remove deep imports from rxjs
|/  
* d5310e4 -- Update project package.json version based on GitLab tag. Done by CI
*   eff306f -- Merge branch '17-replace-dependency-on-kypo-common-with-sentinel-common' into 'master'
|\  
| * 2c0061a -- Replace kypo-common with @sentinel/common
|/  
* 2416f07 -- Update project package.json version based on GitLab tag. Done by CI
*   aca94b8 -- Merge branch '16-update-to-angular-10' into 'master'
|\  
| * ba42b44 -- Resolve "Update to Angular 10"
|/  
* 288c5da -- Update project package.json version based on GitLab tag. Done by CI
### 12.0.1 Update gitlab CI
* eed7790 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9d26cd3 -- Merge branch '34-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c5fdbfc -- Update gitlab CI
|/  
* 7153b7d -- Update project package.json version based on GitLab tag. Done by CI
*   6fdbe4f -- Merge branch '33-update-to-angular-12' into 'master'
|\  
| * 73201e0 -- Update to Angular 12
|/  
* a94cc6a -- Update project package.json version based on GitLab tag. Done by CI
*   c53ba3d -- Merge branch '31-adjust-dtos-and-mappers-according-to-the-new-design-of-questions-in-the-assessment-levels' into 'master'
|\  
| * b7991dc -- Resolve "Adjust DTOs and mappers according to the new design of questions in the assessment levels"
|/  
* 25e1cc9 -- Update project package.json version based on GitLab tag. Done by CI
*   2af45f3 -- Merge branch '32-fix-task-copy-url' into 'master'
|\  
| * 05d40d5 -- Copy task url fixed
|/  
* 16ff17f -- Update project package.json version based on GitLab tag. Done by CI
*   85daa0d -- Merge branch '30-fix-new-line-template-string-blank-spaces' into 'master'
|\  
| * 7b7ff92 -- Fix new line problem
|/  
*   b8ba84b -- Merge branch '29-integrate-adaptive-learning-techniques' into 'master'
|\  
| * e490310 -- Resolve "Integrate Adaptive learning techniques"
|/  
* 63442f9 -- Update project package.json version based on GitLab tag. Done by CI
*   cd242e5 -- Merge branch '27-update-to-angular-11' into 'master'
|\  
| * f79e59b -- Resolve "Update to Angular 11"
|/  
*   04e38d8 -- Merge branch '26-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * e96d821 -- recreate package lock
|/  
* 659a048 -- Update project package.json version based on GitLab tag. Done by CI
*   70b4731 -- Merge branch '25-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 88b2103 -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   2170243 -- Merge branch '24-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 395b55e -- Migrate to eslint, fix lint warnings/errors
|/  
* 0001a11 -- Update project package.json version based on GitLab tag. Done by CI
*   864ec01 -- Merge branch '23-update-dependencies-to-new-format' into 'master'
|\  
| * af218bf -- Update dependencies
|/  
* c27550e -- Update project package.json version based on GitLab tag. Done by CI
*   59633bf -- Merge branch '22-rename-to-kypo-training-api' into 'master'
|\  
| * 9dfc3de -- Rename package
|/  
* f74c4d3 -- Update project package.json version based on GitLab tag. Done by CI
*   699daab -- Merge branch '20-update-endpoint-for-training-events' into 'master'
|\  
| * 394bae6 -- Changed training event uri to match new backend endpoint
|/  
*   0de3e14 -- Merge branch '19-use-cypress-image-in-ci' into 'master'
|\  
| * 4c7c5e9 -- Resolve "Use cypress image in CI"
|/  
* a0f5c7d -- Update project package.json version based on GitLab tag. Done by CI
* 7676143 -- Update project package.json version based on GitLab tag. Done by CI
*   fcacdff -- Merge branch '18-remove-deep-import-from-rxjs' into 'master'
|\  
| * f22ac6c -- Remove deep imports from rxjs
|/  
* d5310e4 -- Update project package.json version based on GitLab tag. Done by CI
*   eff306f -- Merge branch '17-replace-dependency-on-kypo-common-with-sentinel-common' into 'master'
|\  
| * 2c0061a -- Replace kypo-common with @sentinel/common
|/  
* 2416f07 -- Update project package.json version based on GitLab tag. Done by CI
*   aca94b8 -- Merge branch '16-update-to-angular-10' into 'master'
|\  
| * ba42b44 -- Resolve "Update to Angular 10"
|/  
* 288c5da -- Update project package.json version based on GitLab tag. Done by CI
